import ChatServer, SetGame
import threading
import sqlite3

lock = threading.Lock()


class serverRoom(threading.Thread):
    def __init__(self, client):
        threading.Thread.__init__(self)
        self.clients = [client]
        self.setGame = 0

    def add_client(self, client):
        self.clients.append(client)
        ChatServer.chatServer.send_to_clients(self.clients[0], "j,8", self.clients)
        threading.Thread(target=self.listen_to_client(client)).start()

    def get_self(self):
        return self

    def get_clients(self):
        return self.clients

    def decode_client_message(self, data_str, client):
        if data_str[0] == "s":
            print("set game")
            str_arr=data_str[2:].split(',') # arr should be of length 4, indicating the presonce of color, shape ect. in the card stack
            self.setGame = SetGame.setGame(int(str_arr[0]),int(str_arr[1]),int(str_arr[2]),int(str_arr[3]))
            ChatServer.chatServer.send_to_clients(self.clients[0], 'b,' + self.setGame.send_board_to_clients(), self.clients)
            return
        if data_str[0] == "c":
            str_arr=data_str.split(',')
            if str_arr[1] == '1': #code of 3 cards correct
                self.setGame.selected_3_cards([str_arr[3], str_arr[4], str_arr[5]])
                ChatServer.chatServer.send_to_clients(self.clients[0], 'b,' + self.setGame.send_board_to_clients(), self.clients)
                return
            else:
                ChatServer.chatServer.send_to_clients(self.clients[0], data_str, self.clients)
                return
        if data_str[0] == "d":
            self.add_set_to_db(data_str[2:])
            return
        if data_str[0] == "e":
            self.remove_client(client)
        ChatServer.chatServer.send_to_clients(self.clients[0], "hello", self.clients)
        return

    def listen_to_client(self, client):
        print('%s:%s connected.' % client.address)
        while True:
            print("listen.. %s:%s"  % client.address)
            try:
                data = client.socket.recv(1024)
            except:
                print('%s:%s disconnected.' % client.address)
                self.remove_client(client)
                return
            print("data ", ChatServer.chatServer.bytes_to_str(client, data))
            if not data:
                break
            data_str = ChatServer.chatServer.bytes_to_str(client, data)
            self.decode_client_message(data_str, client)
        client.socket.close()
        print('%s:%s disconnected.' % client.address)
        self.remove_client(client)

    def add_set_to_db(self, data_str):
        data_arr = data_str.split(',')
        if len(data_arr)<10:
            return
        con = sqlite3.connect("data.db", isolation_level=None)
        cur = con.cursor()
        data_tup = (data_arr[0],data_arr[1],data_arr[2],data_arr[3],data_arr[4],data_arr[5],data_arr[6],data_arr[7],data_arr[8],data_arr[9])
        cur.execute('''insert into sets values (?,?,?,?,?,?,?,?,?,?)''', data_tup)
        con.close()

    def remove_client(self, client):
        if len(self.clients) == 1:
            ChatServer.chatServer.remove_room(client, self)
        self.clients.remove(client)


    def run(self):
        self.listen_to_client(self.clients[0])
