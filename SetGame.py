import random

CARD_UNUSED = 0
CARD_DISPLAYED = 1
CARD_SELECTED = 2
CARD_USED = 3

random.seed()

class setGame():
    def __init__(self, is_shape, is_shade, is_color, is_number):
        self.all_cards = []
        self.cards_to_show_indexes = []
        self.is_shape = is_shape
        self.is_shade = is_shade
        self.is_color = is_color
        self.is_number = is_number
        # creating a stack of cards
        for i in range(3 if is_shape else 1):
            for j in range(3 if is_shade else 1):
                for k in range(3 if is_color else 1):
                    for l in range(3 if is_number else 1):
                        self.all_cards.append({'shape':i, 'shade':j, 'color': k, 'number': l , 'state':CARD_UNUSED})
        print(self.all_cards,'\n\n\n\n', len(self.all_cards))
        setGame.randomize_12_cards(self)

    def randomize_12_cards(self):
        self.cards_to_show_indexes = self.randomize_n_cards(12, [])
        return self.cards_to_show_indexes

    def randomize_n_cards(self, n, other_12_minus_n_card_indexes):
        n_cards_indexes = []
        for i in range(n):
            index_in_stack = self.randomize_card()
            self.all_cards[index_in_stack]['state'] = CARD_DISPLAYED
            n_cards_indexes.append(index_in_stack)
            other_12_minus_n_card_indexes.append(index_in_stack)
        while not self.is_set_on_screen(other_12_minus_n_card_indexes):
            self.all_cards[n_cards_indexes[-1]]['state'] = CARD_UNUSED
            n_cards_indexes.pop()
            other_12_minus_n_card_indexes.pop()
            index_in_stack = self.randomize_card()
            self.all_cards[index_in_stack]['state'] = CARD_DISPLAYED
            n_cards_indexes.append(index_in_stack)
            other_12_minus_n_card_indexes.append(index_in_stack)
        return n_cards_indexes

    def randomize_card(self):
        while True:
            shape = random.randint(0, 2) if self.is_shape else 0
            shade = random.randint(0, 2) if self.is_shade else 0
            color = random.randint(0, 2) if self.is_color else 0
            number = random.randint(0, 2) if self.is_number else 0
            index_in_stack = self.get_index_in_stack(shape , shade , color , number)
            if self.all_cards[index_in_stack]['state'] == CARD_UNUSED:
                return index_in_stack

    def is_set_on_screen(self, cards_12):
        print(cards_12)
        for i in range(0, 12-2):
            for j in range(i+1, 12-1):
                for k in range(j+1, 12):
                    cards_index = [cards_12[i], cards_12[j], cards_12[k]]
                    #print(self.all_cards[cards_index[0]], self.all_cards[cards_index[1]], self.all_cards[cards_index[2]])
                    if self.is_set(self.all_cards[cards_index[0]], self.all_cards[cards_index[1]], self.all_cards[cards_index[2]]):
                        return True
        return False

    def is_set(self, card_1, card_2, card_3):
        if self.compare_3_cards(card_1, card_2, card_3, 'shape') and self.compare_3_cards(card_1, card_2, card_3, 'shade') and self.compare_3_cards(card_1, card_2, card_3, 'color') and self.compare_3_cards(card_1, card_2, card_3, 'number'):
                print(card_1)
                print(card_2)
                print(card_3)
                return True
        return False

    def compare_3_cards(self, card_1, card_2, card_3, key):
        if card_1[key]==card_2[key] and card_1[key]==card_3[key]:
            return True
        if (not card_1[key] == card_2[key]) and (not card_1[key] == card_3[key]) and (not card_2[key] == card_3[key]):
            return True
        return False

    def get_index_in_stack(self, shape, shade, color, number):
        shape_increase = (3 if self.is_shade else 1) * (3 if self.is_color else 1) * (3 if self.is_number else 1)
        shade_increase = (3 if self.is_color else 1) * (3 if self.is_number else 1)
        color_increase = (3 if self.is_number else 1)
        number_increase = 1
        #if self.is_shape + self.is_shade + self.is_color + self.is_number == 4:
        return shape * shape_increase + shade *shade_increase + color * color_increase + number*number_increase


    def selected_3_cards(self, card_indexes_on_screen): #array of 3 strings
        for i in range(3):
            card_indexes_on_screen[i] = int(card_indexes_on_screen[i])
        answer_dic = self.build_dic_answer(card_indexes_on_screen[0],card_indexes_on_screen[1],card_indexes_on_screen[2])
        unselected_card_indexes = []
        for i in range(12):
            if i not in card_indexes_on_screen:
                unselected_card_indexes.append(self.cards_to_show_indexes[i])
        new_3_cards = self.randomize_n_cards(3, unselected_card_indexes)
        for i in range(3):
            self.all_cards[self.cards_to_show_indexes[card_indexes_on_screen[i]]]['state'] = CARD_USED
            self.cards_to_show_indexes[card_indexes_on_screen[i]] = new_3_cards[i]
        return answer_dic

    def build_dic_answer(self, card1_indexonscreen, card2_indexonscreen, card3_indexonscreen):
        dic = {}
        card1 = self.all_cards[self.cards_to_show_indexes[card1_indexonscreen]]
        card2 = self.all_cards[self.cards_to_show_indexes[card2_indexonscreen]]
        card3 = self.all_cards[self.cards_to_show_indexes[card3_indexonscreen]]
        self.answer_dic_build_attribute(dic, card1, card2, card3, 'shape')
        self.answer_dic_build_attribute(dic, card1, card2, card3, 'shade')
        self.answer_dic_build_attribute(dic, card1, card2, card3, 'color')
        self.answer_dic_build_attribute(dic, card1, card2, card3, 'number')
        print(dic)
        return dic

    def answer_dic_build_attribute(self, dic, card1, card2, card3, key):
        if card1[key] == card2[key] and card1[key] == card3[key]:
            dic[key] = card1[key]
        else:
            dic[key] = -1

    def send_board_to_clients(self):
        card_arr = self.cards_to_show_indexes
        str1 = ""
        for i in range(len(card_arr)):
            str1 = str1 + str(self.all_cards[card_arr[i]]['shape'])
            str1 = str1 + str(self.all_cards[card_arr[i]]['shade'])
            str1 = str1 + str(self.all_cards[card_arr[i]]['color'])
            str1 = str1 + str(self.all_cards[card_arr[i]]['number']) + ','
        print(str1)
        return str1
