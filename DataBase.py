import sqlite3
import datetime

SETS_GAME_ID = 0
SETS_PLAYER_ID = 1
SETS_TO_SETBUTTON_TIME = 2
SETS_SETSELECT_TIME =3
SETS_WRONG_SETS_BEFORE =4
SETS_MISSED_BUTTON_HIT_BEFORE =5
SETS_SHAPE =6
SETS_SHADE =7
SETS_COLOR =8
SETS_NUMBER =9

GAMES_GAME_ID = 0
GAMES_DATE = 1
GAMES_TIME = 2
GAMES_PLAYER_IDS = 3


# return game _id of the new game
def insert_new_game():
    con = sqlite3.connect("data.db", isolation_level=None)
    cur = con.cursor()
    date = datetime.datetime.now().today().strftime("%Y%m%d")
    time = datetime.datetime.now().time()
    cur.execute('''insert into games(date,time,player_ids) values (?,?,?)''', (str(date), str(time), str(8)))
    cur.execute("select * from games where date = '" + (str(date)) + "' and time = '" + str(time) + "'")
    a = cur.fetchall()
    print(a)
    return a[0][0]


def str_of_dic(dic):
    str_ans = ""
    for key in dic.keys():
        str_ans+=key+','+str(dic[key])+';'
    print(str_ans)
    return str_ans


def correct_sets_per_date(player_id):
    dic_date_sets = {}
    con = sqlite3.connect("data.db", isolation_level=None)
    print(111)
    cur = con.cursor()
    cur.execute("select * from sets where player_id = '" + str(player_id)+ "'")
    a = cur.fetchall()
    print(222)
    for answer in a:
        cur.execute("select * from games where game_id = '" + str(answer[SETS_GAME_ID])+ "'")
        date = cur.fetchall()
        date = date[0][GAMES_DATE]
        if date in dic_date_sets.keys():
            dic_date_sets[date] += 1
        else:
            dic_date_sets[date] = 1
    print(dic_date_sets)
    return str_of_dic(dic_date_sets)


def games_per_date(player_id):
    dic_date_games = {}
    con = sqlite3.connect("data.db", isolation_level=None)
    cur = con.cursor()
    cur.execute("select * from games")
    a = cur.fetchall()
    for answer in a:
        if str(player_id) in answer[GAMES_PLAYER_IDS].split(','):
            if answer[GAMES_DATE] in dic_date_games.keys():
                dic_date_games[answer[GAMES_DATE]] += 1
            else:
                dic_date_games[answer[GAMES_DATE]] = 1
    return str_of_dic(dic_date_games)


def incorrect_sets_per_date(player_id):
    dic_date_sets = {}
    con = sqlite3.connect("data.db", isolation_level=None)
    cur = con.cursor()
    cur.execute("select * from sets where player_id = '" + str(player_id)+ "'")
    a = cur.fetchall()
    for answer in a:
        cur.execute("select * from games where game_id = '" + str(answer[SETS_GAME_ID])+ "'")
        date = cur.fetchall()
        date = date[0][GAMES_DATE]
        if date in dic_date_sets.keys():
            dic_date_sets[date]+=answer[SETS_WRONG_SETS_BEFORE]
        else:
            dic_date_sets[date]=answer[SETS_WRONG_SETS_BEFORE]
    return str_of_dic(dic_date_sets)



#correct_sets_per_date(8)
#incorrect_sets_per_date(8)
#games_per_date(8)