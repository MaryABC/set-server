import socket, threading
import ServerRoom
import DataBase

lock = threading.Lock()


class chatServer(threading.Thread):
    rooms = []

    def __init__(self, socket_address):
        threading.Thread.__init__(self)
        print("self ", self)
        self.socket = socket_address[0] #socket
        self.address = socket_address[1] #address

    def run(self):
        to_listen = True
        while to_listen:
            try:
                data = self.socket.recv(1024)
            except:
                print('%s:%s disconnected.' % self.address)
                return
            print("data ", chatServer.bytes_to_str(self, data))
            if not data:
                return
            data_str = chatServer.bytes_to_str(self, data)
            to_listen = self.decode_client_message(data_str)

    def decode_client_message(self, data_str):
        if len(data_str)>=3 and data_str[0:3] == 'GET':
            self.send_to_clients("HTTP/1.1 200 OK\n123",[self])
            print("xxxx")
            return False
        if data_str[0] == 'n':
            game_id = DataBase.insert_new_game()
            self.add_new_room(game_id)
            return False
        if data_str[0] == 'j':
            game_id = data_str.split(',')[1]
            self.add_to_room(game_id)
            return False
        if data_str[0] == 'a':
            data_arr = data_str.split(',')
            print(data_arr)
            if data_arr[1] == '0':
                self.send_to_clients("a,"+DataBase.games_per_date(data_arr[2]), [self])
            if data_arr[1] == '1':
                self.send_to_clients("a,"+DataBase.correct_sets_per_date(data_arr[2]), [self])
            if data_arr[1] == '2': #TO DO: change data base structure so it saves incorrect set number even if a correct set doesn't come after
                self.send_to_clients("a,"+DataBase.incorrect_sets_per_date(data_arr[2]), [self])
        return True

    def add_new_room(self, game_id):
        room = ServerRoom.serverRoom(self)
        room.start()
        self.rooms.append({'game_id': game_id, 'room_obj': room})
        self.send_to_clients("r,"+str(game_id), [self])

    def add_to_room(self, game_id):
        for room in self.rooms:
            if str(room['game_id']) == game_id:
                room['room_obj'].add_client(self)

    def remove_room(self, room_to_remove):
        print("trying to remove..")
        print(room_to_remove)
        for room in self.rooms:
            print(room['room_obj'])
            if room['room_obj'] == room_to_remove:
                self.rooms.remove(room)
                print("removed successfully!")

    def send_to_clients(self, data_str, clients):
        for c in clients:
            try:
                c.socket.send(chatServer.str_to_bytes(self, data_str))
            except:
                print('problem with client ',c)
                clients.remove(c)
            finally:
                print("sent ",data_str)

    def str_to_bytes(self, str_to_encode):
        return str.encode(str_to_encode)

    def bytes_to_str(self, bytes_to_decode):
        return str(bytes_to_decode)[2:-1]
