import os
import sqlite3
import datetime
'''
con = sqlite3.connect("data.db", isolation_level=None)
cur = con.cursor()
#cur.execute("")

today = 14
now = datetime.datetime.now()
print(now)
cur.execute("select * from games")
print(cur.fetchall())

cur.execute('''#insert into games(date) values (?)
''', (now,))
print("select * from games where date = '"+(str(now))+"'")
cur.execute("select * from games where date = '"+(str(now))+"'")
print(cur.fetchall()[0][1])
con.close()
'''

import threading
import time

def worker():
    print(threading.currentThread().getName(), 'Starting')
    time.sleep(2)
    print(threading.currentThread().getName(), 'Exiting')

def my_service():
    print(threading.currentThread().getName(), 'Starting')
    time.sleep(3)
    print(threading.currentThread().getName(), 'Exiting')

t = threading.Thread(name='my_service', target=my_service)
w = threading.Thread(name='worker', target=worker)
w2 = threading.Thread(target=worker) # use default name

w.start()
w2.start()
t.start()

os.system("pause")